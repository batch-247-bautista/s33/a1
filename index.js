

	
//3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API. 
// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console. 

// Make a GET request to retrieve all the todo items
fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(data => {
    // Create an array of strings, with each string containing the index and title of each item
    const titles = data.map((item, index) => `${index}: "${item.title}"`);

    // Join the strings in the array with a line break separator
    const titlesString = titles.join('\n');

    // Print the titles in the console
    console.log(titlesString);
  })
  .catch(error => console.error(error));


// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API. 
// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item. 
	let array = [];

				let arrayMap = array.map(function(data){
					return data = data;
					})
	fetch('https://jsonplaceholder.typicode.com/todos/1')
		// Use the "json" method from the "response" object to convert the data retrieved into JSON format to be used in our application
	.then((response) => response.json())
		// Print the converted JSON value from the 'fetch' request
		// 'promise train' it is the use of multiple ".then" methods
	.then((data) => {
		//console.log('Original Array');
		//console.log(data); // DONE
		//console.log('Result of map method:');
		//console.log(arrayMap);
		console.log(data);
		console.log(`The item ${data.title} on the list has a status of false.`);
	});  // DONE
	

	// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API. 


	fetch('https://jsonplaceholder.typicode.com/todos', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    completed: false,
    id: 201,
    title: 'Created To Do List Item',
    userId: 1,
  })
})
  .then(response => response.json())
  .then(data => console.log(data))
  .catch(error => console.error(error));
	


// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API. 
// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item. 


	fetch('https://jsonplaceholder.typicode.com/todos')

	.then((response) => response.json())
	//.then((data) => console.log(data));


// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API. 
// 9. Update a to do list item by changing the data structure to contain the following properties: 
/*•	Title 
•	Description 
•	Status 
•	Date Completed 
•	User ID */

	fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			dateCompleted: "Pending",
			description: 'To update my to do list with a different data structure',
			id: 1,
			status: "Pending",
			title: 'Updated To Do List Item',
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((data) => console.log(data));


// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API. 
// 11. Update a to do list item by changing the status to complete and add a date when the status was changed. 


	fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			dateCompleted: "07/29/21",
			id: 1,
			status: "Complete",
			title: 'delectus aut autem',
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((data) => console.log(data));


// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API. 

	fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE'
})
  .then(response => {
    if (response.ok) {
      console.log('Item deleted successfully.');
    } else {
      console.error('Unable to delete item.');
    }
  })
  .catch(error => console.error(error));

 /* 13. Create a request via Postman to retrieve all the to do list items. 
•	GET HTTP method 
•	https://jsonplaceholder.typicode.com/todos URI endpoint 
•	Save this request as get all to do list items 
*/

  fetch('https://jsonplaceholder.typicode.com/posts')
		
	.then((response) => response.json())
		
	.then((data) => {
		console.log(data)
	}); 

/*14. Create a request via Postman to retrieve an individual to do list item. 
•	GET HTTP method 
•	https://jsonplaceholder.typicode.com/todos/1 URI endpoint */

fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
	.then((response) => response.json())
	.then((data) => console.log(data));

// 15. Create a request via Postman to create a to do list item. 
// •	POST HTTP method 
// •	https://jsonplaceholder.typicode.com/todos URI endpoint 

fetch('https://jsonplaceholder.typicode.com/posts', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				title: 'New Post',
				body: 'New to do list item!',
				userId: 1
			})
		})
		.then((response) => response.json())
	    .then((data) => console.log(data));


/*16. Create a request via Postman to update a to do list item. 
•	PUT HTTP method 
•	https://jsonplaceholder.typicode.com/todos/1 URI endpoint 
•	Save this request as update to do list item PUT 
•	Update the to do list item to mirror the data structure used in the PUT fetch request */

fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Updated Post!'
		})
	})
	.then((response) => response.json())
	.then((data) => console.log(data));

	/*17. Create a request via Postman to update a to do list item. 
•	PATCH HTTP method 
•	https://jsonplaceholder.typicode.com/todos/1 URI endpoint 
•	Save this request as create to do list item 
•	Update the to do list item to mirror the data structure of the PATCH fetch request 
*/

fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Updated Post using Patch method!'
		})
	})
	.then((response) => response.json())
	.then((data) => console.log(data));

/*18. Create a request via Postman to delete a to do list item. 
•	DELETE HTTP method 
•	https://jsonplaceholder.typicode.com/todos/1 URI endpoint */

fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'DELETE'
	});

